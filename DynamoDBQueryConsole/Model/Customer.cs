﻿using Amazon.DynamoDBv2.DataModel;

namespace DynamoDBQueryConsole.Model
{
    [DynamoDBTable("Customer")]
    public class Customer
    {
        [DynamoDBHashKey("CustomerId")]
        public string CustomerId { get; set; }

        [DynamoDBProperty("FirstName")]
        public string FirstName { get; set; }

        [DynamoDBProperty("LastName")]
        public string LastName { get; set; }
    }
}
