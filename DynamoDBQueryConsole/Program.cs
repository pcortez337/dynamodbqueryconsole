﻿using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using System.Threading.Tasks;
using System.Linq;
using System;
using DynamoDBQueryConsole.Model;


namespace DynamoDBQueryConsole
{
    class Program
    {        
        static void Main(string[] args)
        {
            var client = new AmazonDynamoDBClient("AKIA33V26FKKQHRCGKNJ", "iRPZEi68DLfgkLZr4TadGu+9VHaQth4KFV8neucu", RegionEndpoint.USEast2);
            DynamoDBContext context = new DynamoDBContext(client);

            GetCustomerAsync(context, "307837C4-2AEC-428F-B052-B531C5AA35A7").Wait();
        }

        private static async Task GetCustomerAsync(DynamoDBContext context, string customerId)
        {
            var customer = await context.QueryAsync<Customer>(customerId, new DynamoDBOperationConfig { OverrideTableName = "Customer", IndexName = "CustomerID-index" }).GetRemainingAsync();
            var c = customer.FirstOrDefault();
            Console.WriteLine($"{c.FirstName} {c.LastName}");
            Console.WriteLine("Press any key to continue...");
            Console.ReadLine();
        }

    }
}
